package com.gustavonovais.testluizalabs.di.modules

import com.gustavonovais.testluizalabs.network.RestClient
import org.koin.dsl.module


val retrofitModule = module {
    single { RestClient.build() }
}