package com.gustavonovais.testluizalabs.network

import okhttp3.Interceptor
import okhttp3.Response


class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val builder = request.newBuilder()
        val response: Response? = null

        request = builder.build()

        return response ?: chain.proceed(request)

    }

}