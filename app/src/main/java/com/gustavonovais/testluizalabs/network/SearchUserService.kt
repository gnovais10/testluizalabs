package com.gustavonovais.testluizalabs.network

import com.gustavonovais.testluizalabs.BuildConfig
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline

interface SearchUserService {

    @GET("/1.1/statuses/user_timeline.json")
    fun searchUsers(@Header("Authorization") authorization: String,
                    @Query("screen_name") screenName: String): Single<Response<ArrayList<UserTimeline>>>

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    @POST("oauth2/token")
    fun getToken(
        @Header("Authorization") authorization: String = BuildConfig.basic_auth,
        @Query("grant_type") grant_type: String = "client_credentials"
    ): Single<Response<Token>>
}