package com.gustavonovais.testluizalabs.network

import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("token_type") val token_type: String,
    @SerializedName("access_token") val access_token: String
)