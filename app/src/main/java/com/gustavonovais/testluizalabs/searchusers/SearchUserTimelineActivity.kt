package com.gustavonovais.testluizalabs.searchusers

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.google.cloud.language.v1.AnalyzeSentimentResponse
import com.gustavonovais.testluizalabs.R
import com.gustavonovais.testluizalabs.closeKeyboard
import com.gustavonovais.testluizalabs.di.modules.retrofitModule
import com.gustavonovais.testluizalabs.getCredential
import com.gustavonovais.testluizalabs.isInternetAvailable
import com.gustavonovais.testluizalabs.network.SearchUserService
import com.gustavonovais.testluizalabs.network.Token
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import retrofit2.Retrofit


class SearchUserTimelineActivity : AppCompatActivity(), SearchUsersTimelineContract.View,
    UserTimelineViewHolder.CallbackItemClick {

    private var timelinePresenter: SearchUsersTimelinePresenter? = null
    private val retrofit: Retrofit by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startKoin {
            androidContext(this@SearchUserTimelineActivity)
            modules(retrofitModule)
        }

        configureButtonSearch()
    }

    private fun configureButtonSearch(){
        buttonSearch.setOnClickListener {
            closeKeyboard()

            timelinePresenter = SearchUsersTimelinePresenter(
                this,
                SearchUsersTimelineRepository(retrofit.create(SearchUserService::class.java))
            )
            timelinePresenter?.getToken()
        }
    }

    override fun tokenSuccess(accessToken: Token?) {
        timelinePresenter?.searchUser(editTextViewSearch.text.toString().toLowerCase().trim(), accessToken)
    }

    override fun showUserSearchList(users: ArrayList<UserTimeline>?) {
        users?.apply {
            recyclerViewUsers.adapter =
                UsersTimelineAdapter(this@SearchUserTimelineActivity, users, this@SearchUserTimelineActivity)
            recyclerViewUsers.layoutManager = LinearLayoutManager(this@SearchUserTimelineActivity)
        }
    }

    override fun showError() {
        Snackbar.make(editTextViewSearch, getString(R.string.error_consulting_user), Snackbar.LENGTH_INDEFINITE)
            .setDuration(Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun showErrorToken() {
        Snackbar.make(editTextViewSearch, getString(R.string.error_access), Snackbar.LENGTH_INDEFINITE)
            .setDuration(Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun onItemClick(userTimeline: UserTimeline?) {
        if (isInternetAvailable()) {
            timelinePresenter?.analyzeSentimentResponse(getCredential(), userTimeline)
        } else {
            Snackbar.make(editTextViewSearch, getString(R.string.no_internet_connection), Snackbar.LENGTH_INDEFINITE)
                .setDuration(Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    override fun analyzeSentimentSuccess(sentiment: AnalyzeSentimentResponse?) {
        sentiment.apply {
            this?.documentSentiment?.let {
                timelinePresenter?.getEmojiByScore(it.score)
            }
        }
    }

    override fun analyzeSentimentError(error: String?) {
        error?.apply {
            Snackbar.make(editTextViewSearch, this, Snackbar.LENGTH_INDEFINITE)
                .setDuration(Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    override fun showEmoji(emoji: String?) {
        emoji?.apply {
            Snackbar.make(editTextViewSearch, this, Snackbar.LENGTH_INDEFINITE)
                .setDuration(Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    override fun progress(active: Boolean) {
        progress.visibility = if (active) View.VISIBLE else View.GONE
    }


}
