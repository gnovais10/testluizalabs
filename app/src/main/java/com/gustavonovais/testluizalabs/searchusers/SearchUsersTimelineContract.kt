package com.gustavonovais.testluizalabs.searchusers

import com.google.cloud.language.v1.AnalyzeSentimentResponse
import com.gustavonovais.testluizalabs.network.Token
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import java.io.InputStream

interface SearchUsersTimelineContract {

    interface View {
        fun showError()
        fun showUserSearchList(users: ArrayList<UserTimeline>?)
        fun tokenSuccess(accessToken: Token?)
        fun showErrorToken()
        fun analyzeSentimentSuccess(sentiment: AnalyzeSentimentResponse?)
        fun analyzeSentimentError(error: String?)
        fun showEmoji(emoji : String?)
        fun progress(active : Boolean)
    }

    interface Presenter {
        fun searchUser(userName: String, accessToken: Token?)
        fun analyzeSentimentResponse(ins: InputStream, userTimeline: UserTimeline?)
        fun getEmojiByScore(score: Float)
        fun getToken()
    }

    interface Repository {
        fun searchUser(apiCallbackDefault: SearchUserCallback<ArrayList<UserTimeline>, String>, userName: String, accessToken: Token?)
        fun getToken(apiCallbackDefault: TokenCallback<Token, String>)
        fun analyzeSentimentResponse(ins: InputStream, userTimeline: UserTimeline?,
                                     callback: SearchUsersTimelineContract.AnalyzeSentimentCallback<AnalyzeSentimentResponse, String>)
    }

    interface Interactor {
        fun getEmojiByScore(score: Float) : String?
    }

    interface AnalyzeSentimentCallback<in T, String> {
        fun onErrorAnalyzeSentiment(error: String?)
        fun onSuccessAnalyzeSentiment(response: T)
    }

    interface SearchUserCallback<in RESPONSE, String> {
        fun onErrorSearchUser()
        fun onSuccessSearchUser(response: RESPONSE?)
    }
    interface TokenCallback<in Token, String> {
        fun onErrorToken()
        fun onSuccessToken(response: Token?)
    }

}