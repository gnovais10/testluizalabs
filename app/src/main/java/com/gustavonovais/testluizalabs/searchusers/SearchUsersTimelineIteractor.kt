package com.gustavonovais.testluizalabs.searchusers

class SearchUsersTimelineIteractor : SearchUsersTimelineContract.Interactor {

    override fun getEmojiByScore(score: Float) : String? {
        if (score >= -1 && score <= -0.25) {
            return "\t\uD83D\uDE14 \t\uD83D\uDE14 \t\uD83D\uDE14 \t\uD83D\uDE14 \t\uD83D\uDE14 \t\uD83D\uDE14"
        } else if (score in -0.25..0.25) {
            return "\t\uD83D\uDE10 \t\uD83D\uDE10 \t\uD83D\uDE10 \t\uD83D\uDE10 \t\uD83D\uDE10 \t\uD83D\uDE10"
        } else if (score in 0.25..1.0) {
           return "\t\uD83D\uDE00 \t\uD83D\uDE00 \t\uD83D\uDE00 \t\uD83D\uDE00 \t\uD83D\uDE00 \t\uD83D\uDE00"
        }
        return null
    }


}