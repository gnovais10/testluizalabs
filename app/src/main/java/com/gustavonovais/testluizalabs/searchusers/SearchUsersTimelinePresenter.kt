package com.gustavonovais.testluizalabs.searchusers

import com.google.cloud.language.v1.AnalyzeSentimentResponse
import com.gustavonovais.testluizalabs.network.Token
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import java.io.InputStream

class SearchUsersTimelinePresenter(
    var view: SearchUsersTimelineContract.View,
    var timelineRepository: SearchUsersTimelineRepository
) :
    SearchUsersTimelineContract.Presenter,
    SearchUsersTimelineContract.SearchUserCallback<ArrayList<UserTimeline>, String>,
    SearchUsersTimelineContract.TokenCallback<Token, String>,
    SearchUsersTimelineContract.AnalyzeSentimentCallback<AnalyzeSentimentResponse, String> {


    override fun searchUser(userName: String, accessToken: Token?) {
        view.progress(true)
        timelineRepository.searchUser(this, userName, accessToken)
    }

    override fun onSuccessSearchUser(response: ArrayList<UserTimeline>?) {
        view.progress(false)
        view.showUserSearchList(response)
    }

    override fun onErrorSearchUser() {
        view.progress(false)
        view.showError()
    }

    override fun getToken() {
        timelineRepository.getToken(this)
    }

    override fun onSuccessToken(response: Token?) {
        view.tokenSuccess(response)
    }

    override fun onErrorToken() {
        view.showErrorToken()
    }

    override fun analyzeSentimentResponse(ins: InputStream, userTimeline: UserTimeline?) {
        timelineRepository.analyzeSentimentResponse(ins, userTimeline, this)
    }

    override fun onSuccessAnalyzeSentiment(response: AnalyzeSentimentResponse) {
        view.analyzeSentimentSuccess(response)
    }

    override fun onErrorAnalyzeSentiment(error: String?) {
        view.analyzeSentimentError(error)
    }

    override fun getEmojiByScore(score: Float) {
        view.showEmoji(SearchUsersTimelineIteractor().getEmojiByScore(score))

    }

}