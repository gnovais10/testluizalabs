package com.gustavonovais.testluizalabs.searchusers

import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.language.v1.AnalyzeSentimentResponse
import com.google.cloud.language.v1.Document
import com.google.cloud.language.v1.LanguageServiceClient
import com.google.cloud.language.v1.LanguageServiceSettings
import com.gustavonovais.testluizalabs.network.SearchUserService
import com.gustavonovais.testluizalabs.network.Token
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import java.io.IOException
import java.io.InputStream


open class SearchUsersTimelineRepository constructor(private var service: SearchUserService) :
    SearchUsersTimelineContract.Repository {

    override fun searchUser(
        apiCallbackDefault: SearchUsersTimelineContract.SearchUserCallback<ArrayList<UserTimeline>, String>,
        userName: String, accessToken: Token?
    ) {

        accessToken?.apply {
            val searchUserObservable = service.searchUsers(
                "Bearer ${accessToken.access_token}",
                userName
            )

            searchUserObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<Response<ArrayList<UserTimeline>>> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(listResponse: Response<ArrayList<UserTimeline>>) {
                        if (listResponse.isSuccessful) {
                            listResponse.body()?.apply {
                                apiCallbackDefault.onSuccessSearchUser(this)
                            }
                        } else {
                            listResponse.raw().body()
                            apiCallbackDefault.onErrorSearchUser()
                        }
                    }

                    override fun onError(e: Throwable) {
                        apiCallbackDefault.onErrorSearchUser()
                    }
                })
        }
    }

    override fun getToken(apiCallbackDefault: SearchUsersTimelineContract.TokenCallback<Token, String>) {
        val getToken = service.getToken()

        getToken.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Response<Token>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(listResponse: Response<Token>) {
                    if (listResponse.isSuccessful) {
                        listResponse.body()?.apply {
                            apiCallbackDefault.onSuccessToken(listResponse.body())
                        }
                    } else {
                        apiCallbackDefault.onErrorToken()
                    }
                }

                override fun onError(e: Throwable) {
                    apiCallbackDefault.onErrorToken()
                }
            })
    }


    override fun analyzeSentimentResponse(
        ins: InputStream,
        userTimeline: UserTimeline?,
        callback: SearchUsersTimelineContract.AnalyzeSentimentCallback<AnalyzeSentimentResponse, String>
    ) {
        try {
            val credentialsProvider = FixedCredentialsProvider.create(
                ServiceAccountCredentials.fromStream(ins)
            )

            val languageServiceSettingsBuilder = LanguageServiceSettings.newBuilder()

            val languageServiceSettings = languageServiceSettingsBuilder.setCredentialsProvider(credentialsProvider).build()

            val language = LanguageServiceClient.create(languageServiceSettings)

            val doc = Document.newBuilder()
                .setContent(userTimeline?.text)
                .setType(Document.Type.PLAIN_TEXT)
                .build()


            callback.onSuccessAnalyzeSentiment(language.analyzeSentiment(doc))

        } catch (ex: IOException) {
            callback.onErrorAnalyzeSentiment("Error consulting sentiment api")
        }

    }
}