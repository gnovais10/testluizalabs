package com.gustavonovais.testluizalabs.searchusers

import android.support.v7.widget.RecyclerView
import android.view.View
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import kotlinx.android.synthetic.main.item_timeline_users.view.*


class UserTimelineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    interface CallbackItemClick{
        fun onItemClick(userTimeline: UserTimeline?)
    }

    fun bind(user: UserTimeline?, callbackItemClick: CallbackItemClick) {

        user?.apply {
            itemView.userName.text = user.text

            itemView.setOnClickListener {
                callbackItemClick.onItemClick(this)
            }
        }
    }



}