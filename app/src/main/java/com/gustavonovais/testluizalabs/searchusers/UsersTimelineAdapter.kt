package com.gustavonovais.testluizalabs.searchusers

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gustavonovais.testluizalabs.R
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline

class UsersTimelineAdapter(
    private var activity: Activity, private var users: ArrayList<UserTimeline>, var callbackItemClick: UserTimelineViewHolder.CallbackItemClick
) : RecyclerView.Adapter<UserTimelineViewHolder>() {


    override fun onBindViewHolder(holderTimeline: UserTimelineViewHolder, position: Int) {
        val user = users[position]
        holderTimeline.bind(user, callbackItemClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserTimelineViewHolder {
        val view = LayoutInflater.from(activity).inflate(R.layout.item_timeline_users, parent, false)
        return UserTimelineViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

}