package com.gustavonovais.testluizalabs.searchusers.model

import com.google.gson.annotations.SerializedName

data class Description (

    @SerializedName("urls") val urls : List<String>
)