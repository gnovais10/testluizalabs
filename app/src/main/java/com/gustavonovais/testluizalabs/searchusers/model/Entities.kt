package com.gustavonovais.testluizalabs.searchusers.model

import com.google.gson.annotations.SerializedName

data class Entities (

    @SerializedName("url") val url : Url,
    @SerializedName("description") val description : Description
)