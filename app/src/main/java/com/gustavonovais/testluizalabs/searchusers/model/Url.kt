package com.gustavonovais.testluizalabs.searchusers.model

import com.google.gson.annotations.SerializedName

data class Url (

    @SerializedName("urls") val urls : List<Urls>
)