package com.gustavonovais.testluizalabs

import com.gustavonovais.testluizalabs.searchusers.SearchUsersTimelineContract
import com.gustavonovais.testluizalabs.searchusers.SearchUsersTimelinePresenter
import com.gustavonovais.testluizalabs.searchusers.SearchUsersTimelineRepository
import com.gustavonovais.testluizalabs.searchusers.model.UserTimeline
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SearchUsersPresenterUnitTest {

    @Mock
    private lateinit var view: SearchUsersTimelineContract.View

    @Mock
    private lateinit var repository: SearchUsersTimelineRepository

    @Mock
    private lateinit var userTimeLine: ArrayList<UserTimeline>

    private lateinit var presenter: SearchUsersTimelinePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SearchUsersTimelinePresenter(view, repository)
    }

    @Test
    fun searchUsersTimeLineSuccess(){

        with(presenter){
            this.onSuccessSearchUser(userTimeLine)
        }

        val captor = argumentCaptor<ArrayList<UserTimeline>>()
        Mockito.verify(view).showUserSearchList(capture(captor))

        Assert.assertEquals(userTimeLine, captor.value)
    }

    @Test
    fun searchUsersTimeLineNull(){
        with(presenter){
            this.onSuccessSearchUser(null)
        }

        val captor = argumentCaptor<ArrayList<UserTimeline>>()
        Mockito.verify(view).showUserSearchList(capture(captor))

        Assert.assertEquals(null, captor.value)
    }

}